<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected  $fillable= ['title', 'body', 'category_id', 'user_id', 'image'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
