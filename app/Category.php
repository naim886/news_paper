<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['title','user_id'];

    public function news(){
        return $this->hasMany('App\News');
    }

    public function newsMany(){
        return $this->belongsToMany('App\News');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
