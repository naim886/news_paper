<?php

namespace App\Http\Controllers;

use App\Category;
use App\messages;
use App\News;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners= News::orderBy('id', 'desc')->take(5)->get();

        $national_single_1st=News::where('category_id', '30')->orderBy('id', 'desc')->take(1)->get();
        $national_single_list= News::where('category_id', '30')->orderBy('id', 'desc')->take(5)->skip(1)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $latest_news= News::orderBy('id', 'desc')->take(5)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();

        $political_1st=News::where('category_id', '23')->orderBy('id', 'desc')->take(1)->get();
        $politics= News::where('category_id', '23')->orderBy('id', 'desc')->take(4)->skip(1)->get();

        $techno_1st=News::where('category_id', '22')->orderBy('id', 'desc')->take(1)->get();
        $technology= News::where('category_id', '22')->orderBy('id', 'desc')->take(4)->skip(1)->get();

        $accident_1st=News::where('category_id', '27')->orderBy('id', 'desc')->take(1)->get();
        $accidents= News::where('category_id', '27')->orderBy('id', 'desc')->take(4)->skip(1)->get();

        $international_1st=News::where('category_id', '29')->orderBy('id', 'desc')->take(1)->get();
        $internationals= News::where('category_id', '29')->orderBy('id', 'desc')->take(4)->skip(1)->get();

        $spots_1st=News::where('category_id', '24')->orderBy('id', 'desc')->take(1)->get();
        $sports= News::where('category_id', '24')->orderBy('id', 'desc')->take(4)->skip(1)->get();

        $categories=Category::latest()->get();

        return view('front-end.index', compact('banners','national_single_1st','national_single_list', 'headlines','latest_news','popular_news','political_1st','politics','techno_1st','technology','accident_1st','accidents','international_1st','internationals','spots_1st','sports','categories' ));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Category $category
     * @return void
     */

    public function category($id)
    {

        $newsByCategories= News::where('category_id', $id)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', $id)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
    return view('front-end.pages.category', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));

    }
    public function national()
    {
        $newsByCategories= News::where('category_id', 30)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 30)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.national', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function economics()
    {
        $newsByCategories= News::where('category_id', 26)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 26)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.economics', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }

    public function internationalPage()
    {
        $newsByCategories= News::where('category_id', 29)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 29)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.international', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }

    public function transportAccident()
    {
        $newsByCategories= News::where('category_id', 27)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 27)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.transportAccident', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function transport()
    {
        $newsByCategories= News::where('category_id', 28)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 28)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.transport', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }

    public function entertainment()
    {
        $newsByCategories= News::where('category_id', 25)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 25)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.entertainment', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function sports()
    {
        $newsByCategories= News::where('category_id', 24)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 24)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.sports', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function politics()
    {
        $newsByCategories= News::where('category_id', 23)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 23)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.politics', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function technology()
    {
        $newsByCategories= News::where('category_id', 22)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 22)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.technology', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }

    public function lifeStyle()
    {
        $newsByCategories= News::where('category_id', 21)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 21)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.lifestyle', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function literature()
    {
        $newsByCategories= News::where('category_id', 20)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 20)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.literature', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function village()
{
    $newsByCategories= News::where('category_id', 20)->orderBy('id', 'desc')->get();
    $relatedNews=News::where('category_id', 20)->inRandomOrder()->take(3)->get();
    $headlines= News::orderBy('id', 'desc')->take(20)->get();
    $popular_news= News::inRandomOrder()->take(5)->get();
    $categories=Category::latest()->get();
    return view('front-end.pages.village', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
}
    public function city()
    {
        $newsByCategories= News::where('category_id', 32)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 32)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.city', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }
    public function eastWest()
    {
        $newsByCategories= News::where('category_id', 33)->orderBy('id', 'desc')->get();
        $relatedNews=News::where('category_id', 33)->inRandomOrder()->take(3)->get();
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $popular_news= News::inRandomOrder()->take(5)->get();
        $categories=Category::latest()->get();
        return view('front-end.pages.eastWest', compact('headlines','popular_news','categories','newsByCategories','relatedNews'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        messages::create($request->all());

        session()->flash('status','আপনার বার্তাটি পাঠানো হয়ছে। ধন্যবাদ ');
        return view('front-end.pages.contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $headlines= News::orderBy('id', 'desc')->take(20)->get();
        $single_news=News::findOrFail($id);
        return view('front-end.pages.single', compact('single_news','headlines'));
    }

    public function contact()
    {
        $headlines= News::orderBy('id', 'desc')->take(20)->get();

        return view('front-end.pages.contact', compact('headlines'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function international()
    {
        return view('front-end.pages.international');
    }
}
