<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\NewsRequest;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;

class newsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const UPLOAD_DIR = '/images/news_image/';

    public function index()
    {
        $news=News::latest()->paginate(20);
        return view('dashboard/news/index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::pluck('title', 'id');
        return view('dashboard/news/create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data['image'] = $this->uploadImage($file);
        } else {
            $data['image'] = null;
        }

        $data['user_id'] = auth()->user()->id;

        $news = News::create($data);


        session()->flash('status', 'Post Insert successful!');

        return redirect('dashboard/news');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $news=News::findOrFail($id);

      return view('dashboard/news/show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=News::findOrFail($id);
        $categories=Category::pluck('title', 'id');
        return view('dashboard.news.edit',compact('news','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            //$this->unlinkImage($news->image);
            $file = $request->file('image');
            $data['image'] = $this->uploadImage($file);
        } else {
            $data['image'] = $news->image;
        }

       // $data['updated_by'] = auth()->user()->id;

        $news->update($data);

       // $tag_ids = $request->tag_ids;

        //$post->tags()->sync($tag_ids);

        session()->flash('status', 'News Update successful!');

        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news =News::findOrFail($id);
        // ->where('id', $id)
        // ->first();

        $this->unlinkImage($news->image);



        $news->forceDelete();

        session()->flash('status', 'News Delete successfully');
        return redirect()->route('news.index');
    }


    private function uploadImage($file)
    {

        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();
        Image::make($file)->resize(660, 502)->save(public_path() . self::UPLOAD_DIR . $image_file_name);
        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }

    private function unlinkImage($img)
    {
        if ($img != '' && file_exists(public_path() . self::UPLOAD_DIR . $img)) {
            @unlink(public_path() . self::UPLOAD_DIR . $img);
        }
    }
}
