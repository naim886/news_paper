<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'FrontController@index')->name('front.home');
Route::post('/contact', 'FrontController@store')->name('msg.store');
Route::get('/city', 'FrontController@city')->name('city');
Route::get('/eastWest', 'FrontController@eastWest')->name('eastWest');
Route::get('/economics', 'FrontController@economics')->name('economics');
Route::get('/entertainment', 'FrontController@entertainment')->name('entertainment');
Route::get('/international', 'FrontController@internationalPage')->name('international');
Route::get('/lifeStyle', 'FrontController@lifeStyle')->name('lifeStyle');
Route::get('/literature', 'FrontController@literature')->name('literature');
Route::get('/national', 'FrontController@national')->name('national');
Route::get('/politics', 'FrontController@politics')->name('politics');
Route::get('/single', 'FrontController@international')->name('single');
Route::get('/sports', 'FrontController@sports')->name('sports');
Route::get('/technology', 'FrontController@technology')->name('technology');
Route::get('/transportAccident', 'FrontController@transportAccident')->name('transportAccident');
Route::get('/transport', 'FrontController@transport')->name('transport');
Route::get('/village', 'FrontController@village')->name('village');
Route::get('/contact', 'FrontController@contact')->name('contact');

Route::get('/show/{id}', 'FrontController@show')->name('single.show');

Route::get('/category/{id}', 'FrontController@category')->name('category');

Route::group(['middleware'=>'auth'], function (){
    Route::get('dashboard' , 'DashboardController@index')->name('dashboard.index');
    Route::get('/dashboard/create' , 'DashboardController@create')->name('photos.create');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('dashboard/categories', 'categoriesController@index')->name('category.index');
    Route::get('dashboard/categories/create', 'categoriesController@create')->name('category.create');
    Route::post('dashboard/categories/', 'categoriesController@store')->name('category.store');
    Route::get('dashboard/categories/{id}', 'categoriesController@show')->name('category.show');
    Route::get('dashboard/categories/{id}/edit', 'categoriesController@edit')->name('category.edit');
    Route::put('dashboard/categories/{id}', 'categoriesController@update')->name('category.update');
    Route::delete('dashboard/categories/{id}', 'categoriesController@destroy')->name('category.destroy');



    Route::get('dashboard/news', 'newsController@index')->name('news.index');
    Route::get('dashboard/news/create', 'newsController@create')->name('news.create');
    Route::post('dashboard/news', 'newsController@store')->name('news.store');
    Route::delete('dashboard/news/{id}', 'newsController@destroy')->name('news.destroy');
    Route::get('dashboard/news/{id}', 'newsController@show')->name('news.show');
    Route::put('dashboard/news/{news}', 'newsController@update')->name('news.update');
    Route::get('dashboard/news/{id}/edit', 'newsController@edit')->name('news.edit');

    Route::get('dashboard/message', 'MessageController@index')->name('message.index');
    Route::get('dashboard/message/{id}', 'MessageController@show')->name('message.show');
    Route::delete('dashboard/message/{id}', 'MessageController@destroy')->name('message.destroy');

});

Auth::routes();


