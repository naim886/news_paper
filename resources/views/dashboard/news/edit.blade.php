@extends('dashboard.layouts.master')
@section('page_title','Dashboard')
@section('contentArea')
<div class="col-md-11" style="margin: 0 auto ">

    <h1 class="text-center text-info">Update News</h1>

    {!!Form::model($news,['url'=>'dashboard/news/'.$news->id, 'method'=>'put', 'files'=>true, 'class'=>'form-group'])!!}
    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Please Enter a Title', 'required'=>'required']) !!}
        {!! Form::label('body' , 'Body') !!}
        {!! Form::textarea('body', null, ['class'=>'form-control', 'id'=>'body', 'placeholder'=>'Place Enter The News','required'=>'required']) !!}

        {!! Form::label('image', 'Insert News related Image (660 X 502px recommended) ') !!}
        {!! Form::file('image', ['class'=>'form-control','required'=>'required']) !!}
        {!! Form:: label('category','Category')!!}
        {!! Form::select('category_id', $categories, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Please Enter a Title','required'=>'required']) !!}

        {!! Form::submit('Post News',['class'=>'btn btn-info pull-right']) !!}
        {!!Form::close()!!}
    </div>
</div>

<script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'body' );
</script>

@endsection
