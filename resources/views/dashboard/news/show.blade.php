@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')
    <a href="{{route('news.index')}}"><button class="btn btn-success" role="button">News List</button></a>
<div style="margin: 0 auto; padding: 25px">
    <table class="table-hover" style="margin: 0 auto" cellspacing="20" cellpadding="10">

        <tbody>
        <tr>
            <td><strong>ID</strong></td>
            <td>: {{$news->id}}</td>
        </tr>
        <tr>
            <td><strong>Title</strong></td>
            <td>: {{$news->title}}</td>
        </tr>
        <tr>
            <td><strong>Post</strong></td>
            <td><p style="text-align: justify">: {!! html_entity_decode($news->body) !!}</p> </td>
        </tr>
        <tr>
            <td><strong>Created at</strong></td>
            <td>: {{$news->created_at}}</td>
        </tr>
        <tr>
            <td><strong>Created </strong></td>
            <td>: {{$news->created_at->diffForHumans()}}</td>
        </tr>
        <tr>
            <td><strong>Updated at</strong></td>
            <td>: {{$news->updated_at}}</td>
        </tr>
        <tr>
            <td><strong>Updated</strong> </td>
            <td>: {{$news->updated_at->diffForHumans()}}</td>
        </tr>
        <tr>
            <td><strong>Created By</strong> </td>
            <td>: {{$news->User->name}}</td>
        </tr>
        <tr>
            <td><strong>Image</strong> </td>
            <td>: <img src="{{asset('images/news_image')}}/{{$news->image}}"></td>
        </tr>
        </tbody>
    </table>



</div>

@endsection

