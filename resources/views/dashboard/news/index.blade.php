@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')

    <div class="table-responsive">
        <h1 class="text-center text-info">News List</h1>

        @if(session()->has('status'))

            <div class="alert alert-success text-center">
                {{session('status')}}
            </div>
        @endif


        <table id="mytable" class="table table-bordred table-striped">

            <thead>


            <th>Serial</th>
            <th>Title</th>
            <th>Category</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Show</th>
            <th>Edit</th>
            <th>Delete</th>
            </thead>
            <tbody>
            @php
                $sl=0;
            @endphp
            @foreach($news as $new)
                <tr>

                    <td>{{++$sl}}</td>
                    <td><strong>{{$new->title}}</strong></td>
                    <td>{{$new->Category->title}}</td>
                    <td>{{$new->created_at->toDayDateTimeString()}}</td>
                    <td>{{$new->updated_at->diffForHumans()}}</td>
                    <td><a href="{{url('dashboard/news/'.$new->id )}}" class="btn btn-info center-block glyphicon glyphicon-eye-open btn-sm" role="button" title="Show"></a>
                    </td>
                    <td><a href="{{url('dashboard/news/'.$new->id .'/edit')}}" class="btn btn-success center-block glyphicon glyphicon-pencil btn-sm" role="button" title="Edit"></a>
                    </td>
                    <td>
                        {!! Form::open(['url' => 'dashboard/news/'.$new->id, 'method'=>'delete' ]) !!}
                        {!! Form::button(null, ['type' => 'submit','class'=>'btn btn-danger center-block glyphicon glyphicon-trash btn-sm','title'=>'Delete','onclick' => "return confirm('Are You Sure Want To Delete $new->title ?')"
                                          ]) !!}

                        {!! Form::close() !!}

                    </td>
                </tr>


            @endforeach





            </tbody>

        </table>
        <hr>
        {{$news->links()}}
        <a href="{{route('news.create')}}" class="btn btn-info pull-right" role="button">Add New new</a>
        {{--{{ $news->links() }}--}}
        {{--<div class="clearfix"></div>--}}
        {{--<ul class="pagination pull-right">--}}
        {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
        {{--<li class="active"><a href="#">1</a></li>--}}
        {{--<li><a href="#">2</a></li>--}}
        {{--<li><a href="#">3</a></li>--}}
        {{--<li><a href="#">4</a></li>--}}
        {{--<li><a href="#">5</a></li>--}}
        {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
        {{--</ul>--}}

    </div>







@endsection

