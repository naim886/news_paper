
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="{{route('front.home')}}"><i class="fa fa-dashboard fa-fw text-danger"></i><mark><strong class="text-danger">Live site</strong></mark></a>
            </li>
            <li>
                <a href="{{route('news.index')}}"><i class="fa fa-bar-chart-o fa-fw"></i> News<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{route('news.index')}}">All News</a>
                    </li>
                    <li>
                        <a href="{{route('news.create')}}">New News</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-table fa-fw"></i>All Posts</a>
            </li>
            <li>
                <a href="{{route('message.index')}}"><i class="fa fa-edit fa-fw"></i>Message</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i>Others<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{route('category.index')}}">Category List</a>
                    </li>
                    <li>
                        <a href="{{route('category.create')}}">Add Categroy</a>
                    </li>
                    <li>
                        <a href="#">Notifications</a>
                    </li>
                    <li>
                        <a href="#">Hits</a>
                    </li>

                    <li>
                        <a href="#">Ads Statistics</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <!--
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>

                    </li>
                </ul>

            </li>
            <!-- /.nav-second-level -->
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> User<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">User Profile</a>
                    </li>
                    <li>
                        <a href="#">Logout</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>