@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')

    <h1 class="text-center text-info"> Details of <strong class="text-capitalize text-success">{{$message->name}}</strong>'s Message</h1>

    <table style="padding: 5px; margin: 0 auto" cellspacing="5">

        <tr>
            <td><h3>ID</h3> </td>
            <td><h3>: {{$message->id}}</h3></td>
        </tr>
        <tr>
            <td><h3>Sent By </h3> </td>
            <td><h3>: {{$message->name}}</h3></td>
        </tr>
        <tr>
            <td><h3>Sender E-mail  </h3> </td>
            <td><h3>: {{$message->email}}</h3></td>
        </tr>
        <tr>
            <td><h3>Message   </h3> </td>
            <td><h3>: {{$message->body}}</h3></td>
        </tr>
        <tr>
            <td><h3>Sent at  </h3> </td>
            <td><h3>: {{$message->created_at->toDayDateTimeString()}}</h3></td>
        </tr>




    </table>
    <br>
    <a href="{{route('message.index')}}" class="btn btn-info center-block" role="button">back</a>



@endsection

