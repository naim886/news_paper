@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')

    <div class="table-responsive">
        <h1 class="text-center text-info">Message List</h1>

        @if(session()->has('status'))

            <div class="alert alert-success text-center">
                {{session('status')}}
            </div>
        @endif


        <table id="mytable" class="table table-bordred table-striped">

            <thead>


            <th>Serial</th>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
            <th>Sent at</th>
            <th>Show</th>
            <th>Delete</th>
            </thead>
            <tbody>
            @php
                $sl=0;
            @endphp
            @foreach($messages as $message)
                <tr>

                    <td>{{++$sl}}</td>
                    <td><strong>{{$message->name}}</strong></td>
                    <td>{{$message->email}}</td>
                    <td>{{$message->body}}</td>
                    <td>{{$message->updated_at->toDayDateTimeString()}}</td>
                    <td><a href="{{url('dashboard/message/'.$message->id )}}" class="btn btn-info center-block glyphicon glyphicon-eye-open btn-sm" role="button" title="Show"></a>
                    </td>

                    <td>
                        {!! Form::open(['url' => 'dashboard/message/'.$message->id, 'method'=>'delete' ]) !!}
                        {!! Form::button(null, ['type' => 'submit','class'=>'btn btn-danger center-block glyphicon glyphicon-trash btn-sm','title'=>'Delete','onclick' => "return confirm('Are You Sure Want To Delete $message->title ?')"
                                          ]) !!}

                        {!! Form::close() !!}

                    </td>
                </tr>


            @endforeach





            </tbody>

        </table>
        <hr>
        {{--{{$messages->links()}}--}}

        {{--{{ $categories->links() }}--}}
        {{--<div class="clearfix"></div>--}}
        {{--<ul class="pagination pull-right">--}}
        {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
        {{--<li class="active"><a href="#">1</a></li>--}}
        {{--<li><a href="#">2</a></li>--}}
        {{--<li><a href="#">3</a></li>--}}
        {{--<li><a href="#">4</a></li>--}}
        {{--<li><a href="#">5</a></li>--}}
        {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
        {{--</ul>--}}

    </div>







@endsection

