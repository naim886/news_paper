@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')
<h1 class="text-center">Add New Category</h1>

    {!! Form::open(['url'=>'dashboard/categories', 'method'=>'post', 'class'=>'form-group']) !!}
    {!! Form::text('title',null,['class'=>'form-control','required'=>'required', 'placeholder'=>'Please Insert Category Name']) !!}
<br>
    {!! Form::button('Add Category', ['type'=>'submit', 'class'=>'btn btn-success']) !!}

    {!! Form::close() !!}


@endsection

