@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')
    <h1 class="text-center">Update Category</h1>

    {!! Form::open(['url'=>'dashboard/categories/'.$category->id, 'method'=>'put', 'class'=>'form-group']) !!}
    {!! Form::text('title', $category->title,['class'=>'form-control','required'=>'required']) !!}
    <br>
    {!! Form::button('Update Category', ['type'=>'submit', 'class'=>'btn btn-success']) !!}

    {!! Form::close() !!}


@endsection

