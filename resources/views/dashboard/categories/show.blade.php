@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')

   <h1 class="text-center text-info"> Details of <strong class="text-capitalize text-success">{{$category->title}}</strong></h1>

 <table style="padding: 5px; margin: 0 auto" cellspacing="5">

    <tr>
       <td><h3>ID</h3> </td>
       <td><h3>: {{$category->id}}</h3></td>
    </tr>
    <tr>
       <td><h3>Title </h3> </td>
       <td><h3>: {{$category->title}}</h3></td>
    </tr>
    <tr>
       <td><h3>Created By  </h3> </td>
       <td><h3>: {{$category->user_id}}</h3></td>
    </tr>
    <tr>
       <td><h3>Created at  </h3> </td>
       <td><h3>: {{$category->created_at}}</h3></td>
    </tr>
    <tr>
       <td><h3>Created </h3> </td>
       <td><h3>: {{$category->created_at->diffForHumans()}}</h3></td>
    </tr>
    <tr>
       <td><h3>Updated at  </h3> </td>
       <td><h3>: {{$category->updated_at}}</h3></td>
    </tr>
    <tr>
       <td><h3>Updated </h3> </td>
       <td><h3>: {{$category->updated_at->diffForHumans()}}</h3></td>
    </tr>


 </table>
   <br>
   <a href="{{route('category.index')}}" class="btn btn-info center-block" role="button">back</a>



@endsection

