@extends('dashboard.layouts.master')
@section('page_title','ড্যাশবোর্ড')
@section('contentArea')

<div class="col-md-12">
     <div class="card col-md-4" style="width: 33%; padding: 15px; background-color: silver; text-align: center; float: left">
         <div class="card-body">
             <h2 class="card-title">Total News</h2>
             <h2 class="card-text">{{$post}}</h2>
         </div>
     </div>



    <div class="card col-md-4" style="width: 32%;margin-left: 1%; padding: 15px; background-color: silver; text-align: center; float: left">
        <div class="card-body">
            <h2 class="card-title">Total Comments</h2>
            <h2 class="card-text">50</h2>
        </div>
    </div>


    <div class="card col-md-4" style="width: 32%; margin-left: 1%; padding: 15px; background-color: silver; text-align: center; float: left">
        <div class="card-body">
            <h2 class="card-title">Total Views</h2>
            <h2 class="card-text"> 50</h2>
        </div>
    </div>
</div>
@endsection

