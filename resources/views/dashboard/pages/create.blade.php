@extends('dashboard.layouts.master')
@section('page_title','Dashboard')
@section('contentArea')

   {!!Form::open(['url'=>'dashboard/store', 'method'=>'put'])!!}
    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Please Enter a Title']) !!}
        {!! Form::label('body' , 'Body') !!}
        {!! Form::textarea('Body', null, ['class'=>'form-control', 'id'=>'body', 'placeholder'=>'Pleae Enter The News']) !!}

        {!! Form::label('image', 'Insert News related Image ') !!}
        {!! Form::file('image', ['class'=>'form-control']) !!}
        {!! Form:: label('category','Category')!!}
        {!! Form::select('1', ['National'=>'National', 'international'=>'International']) !!}


       {!! Form::submit('Post News',['class'=>'btn btn-info pull-right']) !!}
       {!!Form::close()!!}
   </div>

@endsection
