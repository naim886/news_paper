@include('dashboard.includes.header')
@include('dashboard.includes.dropdownMSG')
@include('dashboard.includes.dropdowntask')
@include('dashboard.includes.dropdownnotification')
@include('dashboard.includes.dropdownlogin')
@include('dashboard.includes.nav')
@include('dashboard.includes.contentArea')

    @yield('contentArea')

{{--@include('dashboard.includes.notificationArea')--}}
@include('dashboard.includes.footer')
