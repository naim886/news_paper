@extends('front-end.layouts.master')
@section('title','পরিবহন বার্তা । হোম ')
    @section('content')
        @include('front-end.includes.banner')
        @include('front-end.includes.latestNews')
        @include('front-end.includes.national')
        @include('front-end.includes.international')
        @include('front-end.includes.accident')

        @include('front-end.includes.politics')
        @include('front-end.includes.techno')

        @include('front-end.includes.photography')
        @include('front-end.includes.sports')
    @endsection

