@extends('front-end.layouts.master_single')
@section('title','পরিবহন বার্তা । হোম ')

@section('single_content')

{{--<div class="col-md-10 col-md-offset-1" style="padding: 20px 0px 20px 0px; text-align: justify">--}}
    <div class="col-md-10 col-md-offset-1 text-justify">
<div class="img-center">
    <img src="{{asset('images/news_image')}}/{{$single_news->image}}">
    </div>
    <div class="h2 text-center">{{$single_news->title}}</div>

<div class="post_commentbox text-center">
    <a href="#"><i class="fa fa-user"></i>{{$single_news->User->name}}</a>
    <span><i class="fa fa-calendar"> {{$single_news->created_at->toDayDateTimeString()}}</i></span>
    <a href="{{route('category',$single_news->Category->id)}}"><i class="fa fa-tags"></i>{{$single_news->Category->title}}</a> </div>

    <p class="text-justify">

        <br> <br> {!! html_entity_decode($single_news->body) !!}
    </p>
    <hr>
    </div>
{{--</div>--}}

{{--</div>--}}
{{--</section>--}}

@endsection
