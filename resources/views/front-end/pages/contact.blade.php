@extends('front-end.layouts.contact')
@section('title','পরিবহন বার্তা । হোম ')

@section('single_content')
    <div class="col-lg-8 col-md-8 col-sm-8 col-md-offset-2"  style="text-align: center">
        <div class="left_content">
            <div class="contact_area">
                <h2 class="text-center">যোগাযোগ </h2>
                <p class="text-justify">কোন সংবাদ প্রকাশ করতে অথবা কিছু জানার থাকলে অথবা মতামত দিতে আমাদের সাথে যোগাযোগ করুন। </p>

                @if(session()->has('status'))

                    <div class="alert alert-success text-center">
                        {{session('status')}}
                    </div>
                @endif

                {{--<form action="#" class="contact_form">--}}
                    {{--<input class="form-control" type="text" placeholder="Name*">--}}
                    {{--<input class="form-control" type="email" placeholder="Email*">--}}
                    {{--<textarea class="form-control" cols="30" rows="10" placeholder="Message*"></textarea>--}}
                    {{--<input type="submit" value="Send Message">--}}
                {{--</form>--}}
                {!! Form::open(['url'=>'/contact','method'=>'post', 'class'=>'contact_form', 'required'=>'required']) !!}
                {!! Form::text('name', null,['class'=>'form-control', 'placeholder'=>'Name*','required'=>'required']) !!}
                {!! Form::email('email', null,['class'=>'form-control', 'placeholder'=>'Email*','required'=>'required']) !!}
                {!! Form::textarea('body', null,['class'=>'form-control', 'placeholder'=>'Message*', 'cols'=>30,'rows'=>10, 'required'=>'required']) !!}
                {!! Form::button('বার্তা পাঠান',['type'=>'submit','class'=>'form-control']) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection
