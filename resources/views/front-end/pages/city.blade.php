@extends('front-end.layouts.master')
@section('title','পরিবহন বার্তা | নগর জীবন')

@section('content')
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    <div class="single_page">
                        <ol class="breadcrumb">
                            <li><a href="{{route('front.home')}}">হোম</a></li>
                            <li><a href="{{route('national')}}">
                                    নগর জীবন
                                </a></li>

                        </ol>
                        @foreach($newsByCategories as $newsByCategory)
                            <div class="left_content" style="text-align: justify">
                                <h2><a href="{{route('single.show',$newsByCategory->id)}}">{{$newsByCategory->title}}</a> </h2>

                                <div class="post_commentbox">
                                    <a href="#"><i class="fa fa-user"></i>{{$newsByCategory->User->name}}</a>
                                    <span><i class="fa fa-calendar"> {{$newsByCategory->created_at->toDayDateTimeString()}}</i></span>
                                    <a href="{{route('category',$newsByCategory->Category->id)}}"><i class="fa fa-tags"></i>{{$newsByCategory->Category->title}}</a> </div>

                                <div class="single_page_content">
                                    <a href="{{route('single.show',$newsByCategory->id)}}" >
                                        <img class="img-center" src="{{asset('images/news_image')}}/{{$newsByCategory->image}}" alt=""> </a>
                                    <br>
                                    <p style="text-align: justify">{!! html_entity_decode(str_limit($newsByCategory->body, 350)) !!}
                                        <a href="{{route('single.show',$newsByCategory->id)}}" style="color: #843534"><strong>আরও পড়ুন</strong></a> </p>
                                </div>
                            </div>

                        @endforeach



                        <div class="social_link">
                            <ul class="sociallink_nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                @include('front-end.includes.relatedNews')
            </div>



            @endsection/