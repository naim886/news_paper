<div class="single_post_content">
    <h2><span>খেলা</span></h2>
    <div class="single_post_content_left">
        <ul class="business_catgnav">
          @foreach($spots_1st as $sport_1)
            <li>
                <figure class="bsbig_fig  wow fadeInDown"> <a class="featured_img" href="{{route('single.show',$sport_1->id)}}"> <img src="{{asset('images/news_image')}}/{{$sport_1->image}}" alt=""> <span class="overlay"></span> </a>
                    <figcaption> <a href="{{route('single.show',$sport_1->id)}}">{{$sport_1->title}}</a> </figcaption>
                    <p>{!! html_entity_decode(str_limit($sport_1->body, 60)) !!} ...</p>
                </figure>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="single_post_content_right">
        <ul class="spost_nav">

            @foreach($sports as $sport)
            <li>
                <div class="media wow fadeInDown"> <a href="{{route('single.show',$sport->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$sport->image}}"> </a>
                    <div class="media-body"> <a href="{{route('single.show',$sport->id)}}" class="catg_title">
                            {{$sport->title}}</a> </div>
                </div>
            </li>
            @endforeach
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--বাংলাদেশের পারফরম্যান্স দেখে অবাক হাথুরুও</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--সাকিব আছেন, সাকিব নেই!</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--ধাওয়ানের যে রেকর্ড দেখে ঈর্ষা হবে কোহলিরও!</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>
</div>
</div>