<section id="contentSection">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="left_content">
                <div class="single_post_content">
                    <h2><span>জাতীয়</span></h2>
   @foreach($national_single_1st as $single_first)
                    <div class="single_post_content_left">
                        <ul class="business_catgnav  wow fadeInDown">
                            <li>

<figure class="bsbig_fig">
    <a href="{{route('single.show',$single_first->id)}}" class="featured_img">
    <img alt="{{route('single.show',$single_first->id)}}" src="{{asset('images/news_image')}}/{{$single_first->image}}" width="425" >
 <span class="overlay"></span> </a>

    <figcaption>
        <a href="{{route('single.show',$single_first->id)}}">{{ $single_first->title}}</a>
    </figcaption>


    <p style="text-align: justify"> {!! html_entity_decode(str_limit($single_first->body,280)) !!}...</p>
</figure>
                            </li>
                        </ul>
                    </div>
                    @endforeach

@foreach($national_single_list as $single)
                    <div class="single_post_content_right">
                        <ul class="spost_nav">
                            <li>
<div class="media wow fadeInDown"> <a href="{{route('single.show',$single->id)}}" class="media-left">
        <img alt="" src="{{asset('images/news_image')}}/{{$single->image}}" width="72"> </a>
                                    <div class="media-body"> <a href="{{route('single.show',$single->id)}}" class="catg_title">{{$single->title}}</a> </div>
                                </div>
                            </li>

                                {{--<li>--}}
    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left">--}}
            {{--<img alt="" src="{{asset('images/news_image')}}/{{$national_single_3rd->image}}" width="72"> </a>--}}

    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> {{$national_single_3rd->title}} </a> </div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left">--}}
    {{--<img alt="{{$national_single_4th->title}}" src={{asset('images/news_image')}}/{{$national_single_5th->image}}" width="72"> </a>--}}
    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
            {{--{{$national_single_4th->title}}</a> </div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                                {{--<li>--}}
    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left">--}}
            {{--<img alt="" src="{{asset('images/news_image')}}/{{$national_single_5th->image}}" width="72"> </a>--}}
                                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">{{$national_single_5th->title}}</a> </div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                    </div>
    @endforeach
                </div>