<div class="col-lg-4 col-md-4 col-sm-4">
    <div class="latest_post">
        <h2><span>সর্বশেষ সংবাদ</span></h2>
        <div class="latest_post_container">
            <div id="prev-button"><i class="fa fa-chevron-up"></i></div>
            <ul class="latest_postnav">
        @foreach($latest_news as $latest_new)
                <li>
                    <div class="media"> <a href="{{route('single.show',$latest_new->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$latest_new->image}}"> </a>
                        <div class="media-body"> <a href="{{route('single.show',$latest_new->id)}}" class="catg_title">
                               {{$latest_new->title}}</a> </div>
                    </div>
                </li>
                @endforeach
                {{--<li>--}}
                    {{--<div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> কতদিন জেলে থাকবেন খালেদা</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">তিন স্তরে ভোট প্রস্তুতি আওয়ামী লীগে</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> বিএনপি ভীত ও আতঙ্কিত হচ্ছে কেন?</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">নতুন আতঙ্কে রোহিঙ্গারা</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
            <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
        </div>
    </div>
</div>
</div>
</section>