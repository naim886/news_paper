<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/font.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/li-scroller.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/jquery.fancybox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/style.css')}}">.

    <link rel="icon" href="{{asset('images/img/favi.png')}} type="image/gif" sizes="16x16">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>