<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    <header id="header">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="header_top">
                    <div class="header_top_left">
                        <ul class="top_nav">
                            <li><a href="{{route('front.home')}}">হোম</a></li>
                            <li><a href="#">টিকেট বুকিং</a></li>
                            <li><a href="{{route('transportAccident')}}">পরিবহন দুর্ঘটনা </a></li>
                        </ul>
                    </div>
                    <div class="header_top_right">
                        <p>
                            <?php
                            date_default_timezone_set('Asia/Dhaka');
                            echo date('l | d  F , Y | h:i:s A');
                            ?>&nbsp&nbsp&nbsp

    <a href="{{route('home')}}" class="btn btn-primary" role="button">Log in</a>

                        </p>
                    </div>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="header_bottom">
                    <div class="logo_area"><a href="{{route('front.home')}}" class="logo"><img src="{{asset('images/img/logo.png')}}" alt=""></a></div>
                    <div class="add_banner"><a href="#"><h1 style="color: #843534; font-size: 5em;">পরিবহন বার্তা</h1></a></div>
                </div>
            </div>
        </div>
    </header>
    <section id="navArea">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav main_nav">
                    <li class="active"><a href="{{route('front.home')}}"><span class="fa fa-home desktop-home"></span><span class="mobile-show">Home</span></a></li>

                    <li class="dropdown"> <a href="{{route('national')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">জাতীয়</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('national')}}">জাতীয়</a></li>
                            <li><a href="{{route('village')}}">দেশগ্রাম</a></li>
                            <li><a href="{{route('city')}}">নগর জীবন</a></li>
                            <li><a href="{{route('eastWest')}}">পূর্ব-পশ্চিম</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="{{route('national')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">পরিবহন</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('transport')}}">পরিবহন</a></li>
                            <li><a href="{{route('transportAccident')}}">পরিবহন দুর্ঘটনা</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('international')}}">আন্তর্জাতিক</a></li>
                    <li><a href="{{route('economics')}}">অর্থনীতি</a></li>
                    <li><a href="{{route('entertainment')}}">বিনোদন</a></li>
                    <li><a href="{{route('sports')}}">খেলা</a></li>
                    <li><a href="{{route('politics')}}">রাজনীতি</a></li>
                    <li><a href="{{route('technology')}}">বিজ্ঞান ও প্রযুক্তি</a></li>
                    <li><a href="{{route('lifeStyle')}}">জীবনযাপন</a></li>
                    <li><a href="{{route('literature')}}">সাহিত্য</a></li>
                    <li><a href="{{route('contact')}}">যোগাযোগ</a></li>
                    <!-- <li><a href="pages/404.html">404 Page</a></li> -->
                </ul>
            </div>
        </nav>
    </section>