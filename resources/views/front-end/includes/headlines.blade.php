<section id="newsSection">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="latest_newsarea"> <span>শিরোনাম</span>
                <ul id="ticker01" class="news_sticker">
                    @foreach($headlines as $headline)
                    <li><a href="{{route('single.show',$headline->id)}}"><img src="{{asset('images/news_image')}}/{{$headline->image}}" alt="">{{$headline->title}}</a></li>
                    @endforeach
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">বিএনপির কার্যালয়ে মার্কিন দূতাবাসের দুই কর্মকর্তা</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">অরাজক পরিস্থিতি সৃষ্টির শাস্তি ৭ বছর করে দ্রুত বিচার বিল পাস</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">ডিভিশন পেলেন খালেদা</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">অরাজক পরিস্থিতি সৃষ্টির শাস্তি ৭ বছর করে দ্রুত বিচার বিল পাস</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">সাতক্ষীরায় ৭০ লাখ টাকার হীরার গহনা জব্দ</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">টানটান উত্তেজনার মধ্যে বাংলাদেশ কন্স্যুলেটের সামনে পরস্পর বিরোধী কর্মসূচি</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">খালেদা জিয়ার সাজায় পর্তুগালে বিএনপির প্রতিবাদ সভা</a></li>--}}
                    {{--<li><a href="#"><img src="images/news_thumbnail2.jpg" alt="">লন্ডনে বিক্ষোভ সমাবেশ করেছে যুক্তরাজ্য ছাত্রলীগ</a></li>--}}
                </ul>
                <div class="social_area">
                    <ul class="social_nav">
                        <li class="facebook"><a href="#"></a></li>
                        <li class="twitter"><a href="#"></a></li>
                        <li class="flickr"><a href="#"></a></li>
                        <li class="pinterest"><a href="#"></a></li>
                        <li class="googleplus"><a href="#"></a></li>
                        <li class="vimeo"><a href="#"></a></li>
                        <li class="youtube"><a href="#"></a></li>
                        <li class="mail"><a href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>