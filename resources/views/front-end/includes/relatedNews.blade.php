<div class="related_post">
    <h2>সংশ্লিষ্ট সংবাদ<i class="fa fa-thumbs-o-up"></i></h2>
    <ul class="spost_nav wow fadeInDown animated">
        <br>
@foreach($relatedNews as $relatedNew)
        <li>
            <div class="media"> <a class="media-left" href="{{route('single.show',$relatedNew->id)}}"> <img src="{{asset('images/news_image')}}/{{$relatedNew->image}}" alt=""> </a>
                <div class="media-body"> <a class="catg_title" href="{{route('single.show',$relatedNew->id)}}"> {{$relatedNew->title}}</a> </div>
            </div>
        </li>
    @endforeach



    </ul>
</div>