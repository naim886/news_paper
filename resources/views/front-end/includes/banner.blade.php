<section id="sliderSection">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="slick_slider">

                @foreach($banners as $banner)

                <div class="single_iteam"> <a href="pages/single_page.html">
                        <img src="{{asset('images/news_image')}}/{{$banner->image}}" alt=""></a>
                    <div class="slider_article">
                        <h2><a class="slider_tittle" href="pages/single_page.html">{{$banner->title}}</a></h2>
                        <p>{!! html_entity_decode(str_limit($banner->body, 125)) !!} ...</p>
                    </div>
                </div>
@endforeach

                {{--<div class="single_iteam"> <a href="pages/single_page.html"> <img src="images/img/slider_img2.jpg" alt=""></a>--}}
                    {{--<div class="slider_article">--}}
                        {{--<h2><a class="slider_tittle" href="pages/single_page.html">বনানীতে গেস্ট হাউসে ধর্ষণ মামলায় দুই আসামি রিমান্ডে</a></h2>--}}
                        {{--<p>বনানীতে এক নারীকে জন্মদিনের পার্টির কথা বলে গেস্ট হাউসে আমন্ত্রণ করে ধর্ষণের মামলায় দুই অসামিকে দুই দিন করে রিমান্ডে নেওয়ার অনুমাতি দিয়েছেন আদালত। ...</p>--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{--<div class="single_iteam"> <a href="pages/single_page.html"> <img src="images/img/slider_img3.jpg" alt=""></a>--}}
                    {{--<div class="slider_article">--}}
                        {{--<h2><a class="slider_tittle" href="pages/single_page.html">রাজধানীতে ভবন থেকে পড়ে শ্রমিকের মৃত্যু</a></h2>--}}
                        {{--<p>রাজধানীর মোহাম্মদপুর লালমাটিয়ায় নির্মাণাধীন ভবনে থেকে পড়ে মিস্টার (২৬) নামে এক শ্রমিকের মৃত্যু হয়েছে। আজ সকাল ১১টার দিকে দুর্ঘটনা ঘটে। ...</p>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="single_iteam"> <a href="pages/single_page.html"> <img src="images/img/slider_img1.jpg" alt=""></a>--}}
                    {{--<div class="slider_article">--}}
                        {{--<h2><a class="slider_tittle" href="pages/single_page.html">বনানীতে যাত্রীবাহী বাসে অগ্নিকাণ্ড</a></h2>--}}
                        {{--<p>রাজধানীর বনানীতে ২৭ নং ভিআইপি পরিবহন নামের একটি যাত্রীবাহীবাসে অগ্নিকাণ্ডের ঘটনা ঘটেছে। ...</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>


        </div>