<div class="col-lg-4 col-md-4 col-sm-4">
    <aside class="right_content">
        <div class="single_sidebar">
            <h2><span>জনপ্রিয় খবর</span></h2>
            <ul class="spost_nav">
              @foreach($popular_news as $popular_new)
                <li>
                    <div class="media wow fadeInDown"> <a href="{{route('single.show',$popular_new->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$popular_new->image}}"> </a>
                        <div class="media-body"> <a href="{{route('single.show',$popular_new->id)}}" class="catg_title">{{$popular_new->title}}</a> </div>
                    </div>
                </li>
                @endforeach
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">দ্রুত বিচার আইনে সাজা বাড়ল</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">নতুন আতঙ্কে রোহিঙ্গারা</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> কতদিন জেলে থাকবেন খালেদা</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
        <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">বিভাগ</a></li>
                <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">ভিডিও</a></li>
                <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">মন্তব্য</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="category">
                    <ul>
                        @foreach($categories as $category)
                        <li class="cat-item"><a href="{{route('category',$category->id)}}">{{$category->title}}</a></li>
                            @endforeach
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane" id="video">
                    <div class="vide_area">
                        <iframe width="100%" height="250" src="#" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="comments">
                    <ul class="spost_nav">
                        <li>
                            <div class="media wow fadeInDown"> <a href="#" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                                <div class="media-body"> <a href="#" class="catg_title"> Comments</a> </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="single_sidebar wow fadeInDown">
            <h2><span>স্পন্সর </span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
        <div class="single_sidebar wow fadeInDown">
            <h2><span>আর্কাইভ</span></h2>

        </div>
        <div class="single_sidebar wow fadeInDown">
            <h2><span>লিংক</span></h2>
            <ul>
                <li><a href="#">ব্লগ</a></li>

            </ul>
        </div>
    </aside>
</div>
</div>
</section>