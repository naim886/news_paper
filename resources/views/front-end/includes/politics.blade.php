<div class="fashion_technology_area">
    <div class="fashion">
        <div class="single_post_content">
            <h2><span>রাজনীতি</span></h2>
            <ul class="business_catgnav wow fadeInDown">
    @foreach($political_1st as $political_1sts)
                <li>
                    <figure class="bsbig_fig">
                        <a href="{{route('single.show',$political_1sts->id)}}" class="featured_img"> <img alt="" src="{{asset('images/news_image')}}/{{$political_1sts->image}}"> <span class="overlay"></span> </a>
                        <figcaption> <a href="{{route('single.show',$political_1sts->id)}}">{{$political_1sts->title}}</a> </figcaption>
                        <p>{!!  html_entity_decode(str_limit($political_1sts->body, 60)) !!}...</p>
                    </figure>
                </li>
@endforeach
            </ul>

            <ul class="spost_nav">
@foreach($politics as $politic)
                <li>
                    <div class="media wow fadeInDown"> <a href="{{route('single.show',$politic->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$politic->image}}"> </a>
                        <div class="media-body"> <a href="{{route('single.show',$politic->id)}}" class="catg_title"> {{$politic->title}}</a> </div>
                    </div>
                </li>
                @endforeach
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">ডিপ্লোমা নার্স ও চারজন নারী কারারক্ষী আছেন</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                                {{--প্রশ্নপত্র ফাঁস ঠেকাতে ইন্টারনেটের গতি কমছে</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                        {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> প্রশ্নপত্র ফাঁস ঠেকাতে মোবাইল ইন্টারনেট আজ রোববার সকালে আধা ঘণ্টার জন্য বন্ধ ছিল</a> </div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>