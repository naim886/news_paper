<div class="technology">
    <div class="single_post_content">
        <h2><span>পরিবহন দুর্ঘটনা</span></h2>
        <ul class="business_catgnav">
@foreach($accident_1st as $accident_1)
            <li>
                <figure class="bsbig_fig wow fadeInDown"> <a href="{{route('single.show',$accident_1->id)}}" class="featured_img"> <img alt="" src="{{asset('images/news_image')}}/{{$accident_1->image}}"> <span class="overlay"></span> </a>
                    <figcaption> <a href="{{route('single.show',$accident_1->id)}}">
                            {{$accident_1->title}}</a> </figcaption>
                    <p>{!! html_entity_decode(str_limit($accident_1->body, 60)) !!}...</p>
                </figure>
            </li>
@endforeach
        </ul>
        <ul class="spost_nav">
@foreach($accidents as $accident)
            <li>
                <div class="media wow fadeInDown"> <a href="{{route('single.show',$accident->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$accident->image}}"> </a>
                    <div class="media-body"> <a href="{{route('single.show',$accident->id)}}" class="catg_title">
                           {{$accident->title}}</a> </div>
                </div>
            </li>
@endforeach


            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--হুয়াওয়ের নোভা টুআইয়ে নতুন ফিচার</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--ফেব্রুয়ারিতে ৮ স্মার্টফোন চমক</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> আইফোনে আসছে নতুন সুবিধা</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>
</div>