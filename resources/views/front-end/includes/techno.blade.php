<div class="technology">
    <div class="single_post_content">
        <h2><span>বিজ্ঞান ও প্রযুক্তি</span></h2>
        <ul class="business_catgnav">
@foreach($techno_1st as $technos)
            <li>
                <figure class="bsbig_fig wow fadeInDown"> <a href="{{route('single.show',$technos->id)}}" class="featured_img"> <img alt="" src="{{asset('images/news_image')}}/{{$technos->image}}"> <span class="overlay"></span> </a>
                    <figcaption> <a href="{{route('single.show',$technos->id)}}">
                            {{$technos->title}}</a> </figcaption>
                    <p>{!! html_entity_decode(str_limit($technos->body, 60)) !!}...</p>
                </figure>
            </li>
@endforeach
        </ul>
        <ul class="spost_nav">
@foreach($technology as $techno)
            <li>
                <div class="media wow fadeInDown"> <a href="{{route('single.show',$techno->id)}}" class="media-left"> <img alt="" src="{{asset('images/news_image')}}/{{$techno->image}}"> </a>
                    <div class="media-body"> <a href="{{route('single.show',$techno->id)}}" class="catg_title">
                           {{$techno->title}}</a> </div>
                </div>
            </li>
@endforeach


            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--হুয়াওয়ের নোভা টুআইয়ে নতুন ফিচার</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title">--}}
                            {{--ফেব্রুয়ারিতে ৮ স্মার্টফোন চমক</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>--}}
                    {{--<div class="media-body"> <a href="pages/single_page.html" class="catg_title"> আইফোনে আসছে নতুন সুবিধা</a> </div>--}}
                {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>
</div>