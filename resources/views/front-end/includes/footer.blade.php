<footer id="footer">
    <div class="footer_top">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInLeftBig">
                    <h2 style="text-align: center">যোগাযোগ</h2>
                    <address style="text-align: center">
                       টুইন টাওয়ার, গুলিস্তান, ঢাকা, বাংলাদেশ <br> ফোনঃ ০১৭১১-৯৮০২১৩  <br> ই-মেইল : info@paribahanbarta.com
                    </address>
                    <div style="width: 350px"><iframe width="350" height="200" src="https://maps.google.com/maps?width=350&amp;height=200&amp;hl=en&amp;q=gulistan%2C%20Dhaka%2C%20Bangladesh+(%E0%A6%AA%E0%A6%B0%E0%A6%BF%E0%A6%AC%E0%A6%B9%E0%A6%A8%20%E0%A6%AC%E0%A6%BE%E0%A6%B0%E0%A7%8D%E0%A6%A4%E0%A6%BE)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe></div><br />
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInDown">
                    <h2 style="text-align: center">গুরুত্বপূর্ণ লিঙ্ক </h2>
                    <ul class="tag_nav" style="text-align: center">
                        <li><a href="{{route('sports')}}">খেলাধুলা </a></li>
                        <li><a href="{{route('politics')}}">রাজনীতি</a></li>
                        <li><a href="{{route('economics')}}">বাণিজ্য</a></li>
                        <li><a href="{{route('lifeStyle')}}">জীবনযাপন</a></li>
                        <li><a href="{{route('literature')}}">সাহিত্য </a></li>
                        <li><a href="{{route('technology')}}">বিজ্ঞান ও প্রযুক্তি</a></li>
                        <li><a href="{{route('entertainment')}}">বিনোদন</a></li>
                        <li><a href="{{route('transportAccident')}}">পরিবহন দুর্ঘটনা</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInRightBig">
                    <h2 style="text-align: center">আমাদের সম্পর্কে </h2>
                        <h2>সবার আগে সব দিকের সব খবর জানতে ভিজিট করুন পরিবহন বার্তা ।  </h2>
                    <ul class="tag_nav" style="text-align: center">
                      <li><a href="#">আমাদের সম্পর্কে </a></li>
                      <li><a href="#">বিজ্ঞাপন নীতিমালা </a></li>
                      <li><a href="#">বিজ্ঞাপন মূল্য তালিকা  </a></li>
                      <li><a href="#">সম্পাদকীয়</a></li>
                       <li><a href="{{route('contact')}}">যোগাযোগ</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <p class="copyright">Copyright &copy; {{date('Y')}} <a href="{{route('front.home')}}">পরিবহন বার্তা</a></p>
        <p class="copyright" style="text-align: right !important;">Developed By <a href="{{url('http://www.jubaersoft.com/')}}"  target="_blank">JubaerSoft</a></p>
    </div>
</footer>
</div>
<script src="{{asset('front-end/js/jquery.min.js')}}"></script>
<script src="{{asset('front-end/js/wow.min.js')}}"></script>
<script src="{{asset('front-end/js/bootstrap.min.js')}}"></script>
<script src="{{asset('front-end/js/slick.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.li-scroller.1.0.js')}}"></script>
<script src="{{asset('front-end/js/jquery.newsTicker.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('front-end/js/custom.js')}}"></script>


</body>
</html>