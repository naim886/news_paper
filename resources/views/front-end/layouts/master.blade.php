@include('front-end.includes.header')
@include('front-end.includes.nav')
@include('front-end.includes.headlines')

@yield('content')

@include('front-end.includes.popularNews')
@include('front-end.includes.footer')