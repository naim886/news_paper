@include('front-end.includes.header')
@include('front-end.includes.nav')
@include('front-end.includes.headlines')

@yield('single_content')


@include('front-end.includes.footer')